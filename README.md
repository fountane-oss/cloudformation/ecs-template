# AWS ECS Cloudformation Template  

This is a release candidate of the cloudformation template to deploy container workloads in aws ecs. 

You can spin up the infrastructure by running the template in this order.  

```
- vpc.yml
- security_groups.yml
- iam.yml
- rds.yml
- app-cluster.yml
- api.yml 
```  

Please do make note of the respective environment variables and update/add them wherever needed. 

The template will prompt you for any if needed.  